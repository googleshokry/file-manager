<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function getAllFiles()
    {
        $allFiles = Storage::disk('browser')->allFiles();
        $files = array();

        foreach ($allFiles as $file) {

            $files[] = $this->fileInfo(pathinfo(storage_path('app/browser').'/'  . $file));
        }

        return view('files', compact('files'));
    }

    public function fileInfo($filePath)
    {
        $file = array();
        $file['name'] = $filePath['filename'];
        $file['extension'] = $filePath['extension'];
        $file['size'] = filesize($filePath['dirname'] . '/' . $filePath['basename']);

        return $file;
    }

    function download($filename)
    {
        $path = storage_path('app/browser/'.$filename);
        if (file_exists($path)) {
            return \Response::download($path);
        }
    }
}
